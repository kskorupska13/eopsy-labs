#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#define N 		5 //no of philosophers
#define LEFT 		( i + N - 1 ) % N
#define RIGHT 		( i + 1 ) % N
#define MEALS_NO 	3 //no of meals

//states philosophers can be in for state[N]
#define THINKING 	0 
#define HUNGRY  	1
#define EATING 		2

//times of sleep
#define THINKING_TIME 	1 
#define EATING_TIME 	2
#define RUNTIME		30

// main mutex
pthread_mutex_t	m = PTHREAD_MUTEX_INITIALIZER; 		//initialized to 1
int state[N];	//states of philosophers initiated to 0
pthread_mutex_t s[N] = 	PTHREAD_MUTEX_INITIALIZER; 	//initialized to 0's philosophers mutexes


void grab_forks(int i); //function to grab forks thinking->hungry->eating

void put_away_forks(int i); //function to put away forks eating->thinking

void test(int i); // test the state of the phil. (if he is hungry) and its neighbours (not eating) + unlock the mutex for this phil. 

void init_philosophers(int *ids, pthread_t *philosophers); //create the threads defined in pthread_t array with ids

void *philosopher(void *id); // method executed in thread

int  main(int argc, char** argv){
	
	//Initialize to 0's the s[N] mutexes - locked state
	//and states to THINKING
	for(int i=0; i<N; i++){
		pthread_mutex_lock(&s[i]);
		state[i]=THINKING;	
	}
	
	//Create philosopher threads
	pthread_t philosophers[N]; //philosopher threads
	int phi_nums[N] = {0,1,2,3,4}; //philosopher ids
	init_philosophers(phi_nums, philosophers); //pthread_create
	sleep(RUNTIME);
	//End 
	printf("Ending threads\n");
	for(int i=0; i<N; i++){
	
		//Cancel all threads if they are still running
		pthread_cancel(philosophers[i]);
		//Join back the threads
		if(pthread_join(philosophers[i],NULL)){
		
			fprintf(stderr, "\nERROR: Failed joining thread!\n");
			exit(1);	
			
		}	
		printf("Philosopher no. %d left the table\n", i);
	}
	return 0;


}

void init_philosophers(int *ids, pthread_t *philosophers){
	for(int i=0; i<N; i++){
		//ref to phread_t, null , the pointer to function, argument to function
		if(pthread_create(&philosophers[i],NULL, philosopher,(void *)&ids[i])!=0)
		{
			fprintf(stderr, "\nERROR: Failed creating a philosopher thread!\n");
			exit(2);
		}		
	}	

}

void *philosopher(void *id){
	
	int* i = (int*) id;	
	printf("Philosopher no. %d sat at the table\n", *i);
	int meals;
	for(meals=0; meals<MEALS_NO; meals++){
	
		//THINKING		
		printf("Philosopher no. %d is thinking.\n", *i);
		sleep(THINKING_TIME);
		//HUNGRY
		grab_forks(*i);
		//EATING
		printf("Philosopher no. %d is eating meal no. %d.\n", *i, meals);
		sleep(EATING_TIME);
		//End eating and think again
		put_away_forks(*i);
	}
	printf("Philosopher no. %d ate all the meals served.\n", *i);
	return NULL;

}

void grab_forks( int i ){
	//lock the mutex 
	pthread_mutex_lock( &m );
		state[i] = HUNGRY;
		printf("Philosopher no. %d is hungry. and grabs the forks.\n",i);
		test( i );
	//unlock the mutex 
	pthread_mutex_unlock( &m );
	pthread_mutex_lock( &s[i] );
}

void put_away_forks( int i ){
	//lock the mutex 
	pthread_mutex_lock( &m );
		state[i] = THINKING;
		printf("Philosopher no. %d is thinking and puts away the forks.\n",i);
		test( LEFT );
		test( RIGHT );
	//unlock the mutex 
	pthread_mutex_unlock( &m );
}

void test( int i ){
	// test if the philosopher is hungry and if the neighbors are not eating
	if( state[i] == HUNGRY && state[LEFT] != EATING && state[RIGHT] != EATING )
	{
		//the left and right neighbors are not eating
		state[i] = EATING;
		pthread_mutex_unlock( &s[i] );
	}
}
