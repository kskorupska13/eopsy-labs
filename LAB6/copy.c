#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

//copy [-m] <file_name> <new_file_name>
//copy [-h]

void copy_read_write(int fd_from, int fd_to);
void copy_mmap(int fd_from, int fd_to);
int file_size(int fd);

int main(int argc, char **argv){


	if(argc==1){
		printf("No arguments provided! Use 'copy -h' for help!\n");
		return 1;
	}

	int a;
	int m_arg = 0;
	opterr = 0;//preventing error message

	while((a = getopt(argc,argv,"hm"))!=-1){
		switch(a){
			case 'h':
				printf("Usage:\n");
				printf(" copy <file_name> <new_file_name>\n");
				printf(" copy [-m] <file_name> <new_file_name>\n");
				printf("[-m] - map files to memory regions with mmap() and copy the file with memcpy()\n");
				return 0;
			case 'm':
				if(argc != 4){
				    printf("Option -m requires 2 arguments.\n Use 'copy -h' for help!\n");
				    return 1;
				}
				m_arg = 1;
				break;
		    	case '?': //when ? character is stored in optopt
				if(isprint(optopt)) //check if it is a character
				    printf("Unknown option '-%c'\nUse 'copy -h' for help!\n", optopt);
				else
				    printf("Unknown option character `\\x%x`.\n", optopt);
				return 1;
		    	default:
				fprintf(stderr, "copy: abort()");
				abort();
		}
	    }

	if(argc != 3 && m_arg == 0){
		printf("Wrong arguments list\nUse 'copy -h' for help!\n");
		return 1;
	}

	int fd_from, fd_to;

	//open files
	if((fd_from = open(argv[m_arg+1], O_RDONLY)) == -1 || (fd_to = open(argv[m_arg+2], O_RDWR | O_CREAT, 0666)) == -1){
		perror("open");
		return 1;
	}//check if opened successfully

	//choose right copy function
	if(m_arg == 0)
		copy_read_write(fd_from, fd_to);
	else
		copy_mmap(fd_from, fd_to);

	//close files
	close(fd_from);
	close(fd_to);
	return 0;
}


int file_size(int fd){

      off_t current = lseek(fd, (off_t)0, SEEK_CUR);
      off_t fs = lseek(fd, (off_t)0, SEEK_END);
      lseek(fd, current, SEEK_SET);
      return (int)fs;
}

void copy_mmap(int fd_from,int fd_to){

    struct stat sbuf;//stat structure for fstat
    char *from;
    char *to;
    if(fstat(fd_from, &sbuf) == -1){
        perror("fstat");
        exit(1);
    }// checking if fsize succeeded

    from = mmap(NULL, sbuf.st_size, PROT_READ, MAP_PRIVATE, fd_from, 0);//address 1

    if(from == MAP_FAILED){
        perror("mmap");
        exit(1);
    } //checking if mmap succeeded

    if(ftruncate(fd_to, sbuf.st_size) == -1){
        perror("ftruncate");
        exit(1);
    }//checking if truncate to size succeeded

    to = mmap(NULL, sbuf.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd_to, 0);//address 2 can be also written to

    if(to == MAP_FAILED){
        perror("mmap");
        exit(1);
    }//checking if mmap succeeded

    memcpy(to, from, sbuf.st_size); //copy characters from address 1 to address 2
    //Without use msync there is no guarantee that changes are written back
    if(msync(to, sbuf.st_size, MS_SYNC) == -1){
        perror("msync");
        exit(1);//check if file is synchronized with memory map
    }

}

void copy_read_write(int fd_from, int fd_to){

    int fs = file_size(fd_from); //file size

    char buf[fs];//buffer

    int in, out;

    while((in = read(fd_from, buf, fs)) > 0){
	out = write(fd_to, buf, (ssize_t) in);
    }//read and write

}






